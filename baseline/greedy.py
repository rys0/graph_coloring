# coding: UTF-8
import numpy as np
import networkx as nx
import matplotlib.pyplot as plt


def generate_graph(node_number, connectivity_rate=0.4):
    g = 0
    while np.sum(g) == 0:
        g = np.random.rand(node_number, node_number)
        g = g + g.T - 2 * np.eye(node_number)
        g = np.where(g > (1-connectivity_rate)*2, 1, 0)
    return g


def greedy_coloring(graph, n_colors):
    n_nodes = len(graph)
    count = 0
    colors = np.arange(2, n_colors+2)
    nodes = np.zeros(n_nodes, dtype=int)
    for n in range(n_nodes):
        for c in colors:
            if c not in graph[n]:
                graph[:, n] *= c
                nodes[n] = c-1
                count += 1
                break
    return nodes, count/n_nodes


def draw_graph(graph, nodes):
    G = nx.from_numpy_matrix(graph)
    nx.draw_networkx(G, with_labels=True, node_color=nodes, cmap=plt.cm.Set1)
    plt.axis("off")
    plt.show()


def main():
    node_number = 9
    n_colors = 3
    N = 10000
    p = 0
    for i in range(N):
        graph = generate_graph(node_number)
        nodes, percentage = greedy_coloring(graph.copy(), n_colors)
        p += percentage
    print("filled percentage:", p/N)
    draw_graph(graph, nodes)


if __name__ == '__main__':
    main()
