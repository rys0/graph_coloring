# coding: utf-8
import torch.nn as nn
import torch.nn.functional as F
from layers import GraphConvolution


class GCN(nn.Module):
    def __init__(self, nfeat, nhid, nclass, dropout):
        super(GCN, self).__init__()

        self.gc1 = GraphConvolution(nfeat, nhid)
        self.gc2 = GraphConvolution(nhid, nclass)
        self.dropout = dropout

    def forward(self, x, adj):
        x = F.relu(self.gc1(x, adj))
        x = F.dropout(x, self.dropout, training=self.training)
        x = self.gc2(x, adj)
        return x.view(-1)


class ResGCN(nn.Module):
    def __init__(self, n_nodes, dropout):
        super(ResGCN, self).__init__()
        self.gcs = nn.ModuleList(
            [GraphConvolution(n_nodes[i], n_nodes[i+1]) for i in range(len(n_nodes)-1)])
        #self.fc = nn.Linear(n_nodes[0], n_nodes[-1])
        self.fcs = nn.ModuleList(
            [nn.Linear(n_nodes[i], n_nodes[i+1]) for i in range(len(n_nodes)-1)])
        self.dropout = dropout

    def forward(self, x, adj):
        res = x
        for fc in self.fcs[:-1]:
            res = F.relu(fc(res))
            res = F.dropout(res, self.dropout, training=self.training)
        for gc in self.gcs[:-1]:
            x = F.relu(gc(x, adj))
            #x = F.dropout(x, self.dropout, training=self.training)
        x = self.gcs[-1](x, adj) + self.fcs[-1](res)
        return x.view(-1)
