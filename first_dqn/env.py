# coding: utf-8
import numpy as np


class GraphColoring():

    def __init__(self, n_colors, min_nodes, max_nodes):
        self.n_colors = n_colors
        self.min_nodes = min_nodes
        self.max_nodes = max_nodes

        self.n_nodes = np.random.randint(min_nodes, max_nodes)
        self.adj = self.generate_graph(self.n_nodes)
        self.nodes = np.array([[False]*self.n_colors]*self.n_nodes)
        self.nodes_remain = self.n_nodes
        self.action_space_num = self.n_colors * self.n_nodes

    def step(self, action):
        """
        action: pos = action / n_colors, col = action % n_colors
        """
        is_done = True
        pos = action // self.n_colors
        col = action % self.n_colors
        if self.check_valid(pos, col):
            reward = 1 - self.nodes_remain/self.n_nodes
            self.nodes_remain -= 1
            self.nodes[pos, col] = True
            if self.nodes_remain != 0:
                is_done = False
        else:
            reward = -self.nodes_remain/self.n_nodes

        # astypeで返しているのでこのクラスが保持しているものとは別のオブジェクトが返される.
        return self.nodes.astype(float), reward, is_done, None

    def check_valid(self, pos, color):
        """
        adj: グラフの隣接行列
        graph: 現在のグラフの状態(各ノードの色)
        pos: 置きたい場所
        color: 置きたい色
        """
        if self.nodes[pos].any():
            return False

        return not(self.nodes[self.adj[pos] == 1].any(axis=0)[color])

    def random_sample(self):
        return np.random.choice(self.action_space_num)

    def reset(self):
        self.n_nodes = np.random.randint(self.min_nodes, self.max_nodes)
        self.adj = self.generate_graph(self.n_nodes)
        self.nodes = np.array([[False]*self.n_colors]*self.n_nodes)
        self.nodes_remain = self.n_nodes
        self.action_space_num = self.n_colors * self.n_nodes
        return self.adj, self.nodes.astype(float)

    def generate_graph(self, node_number, connectivity_rate=0.4):
        g = 0
        while np.sum(g) == 0:
            g = np.random.rand(node_number, node_number)
            g = g + g.T - 2 * np.eye(node_number)
            g = np.where(g > (1-connectivity_rate)*2, 1, 0)
        return g
