# coding: utf-8
import collections
import argparse
import time

import gym
import gym.spaces
import numpy as np
import torch
from scipy import sparse
from tensorboardX import SummaryWriter
from torch import nn, optim

from models import GCN, ResGCN
from utils import normalize, sparse_mx_to_torch_sparse_tensor
from env import GraphColoring
from graph_visualizer import GraphVisualizer

MEAN_REWARD_BOUND = 19.5

GAMMA = 0.99
BATCH_SIZE = 32
REPLAY_SIZE = 10000
LEARNING_RATE = 1e-4
SYNC_TARGET_FRAMES = 1000
REPLAY_START_SIZE = 10000
STEP_NUM = 120000

EPSILON_DECAY_LAST_FRAME = 100000
EPSILON_START = 1.0
EPSILON_FINAL = 0.02


Experience = collections.namedtuple('Experience', field_names=[
                                    'state', 'action', 'reward', 'done', 'new_state', 'adj'])


class ExperienceBuffer:
    def __init__(self, capacity):
        self.buffer = collections.deque(maxlen=capacity)

    def __len__(self):
        return len(self.buffer)

    def append(self, experience):
        self.buffer.append(experience)

    def sample(self, batch_size):
        indices = np.random.choice(len(self.buffer), batch_size, replace=False)
        states, actions, rewards, dones, next_states, adjs = zip(
            *[self.buffer[idx] for idx in indices])
        return np.array(states), np.array(actions), np.array(rewards, dtype=np.float32), \
            np.array(dones, dtype=np.uint8), np.array(next_states), adjs


class Agent:
    def __init__(self, env, exp_buffer, device="cpu"):
        self.env = env
        self.exp_buffer = exp_buffer
        self.device = device
        self._reset()

    def _reset(self):
        adj, self.state = env.reset()
        self.adj = sparse_mx_to_torch_sparse_tensor(
            normalize(sparse.csr_matrix(adj.astype(float)))).to(self.device)
        self.total_reward = 0.0

    def play_step(self, net, epsilon=0.0):
        done_reward = None
        percentage = None

        # epsilon-greedy
        if np.random.random() < epsilon:
            action = env.random_sample()
        else:
            state_a = np.array(self.state, copy=False)
            state_v = torch.FloatTensor(state_a).to(device)
            q_vals_v = net(state_v, self.adj)
            _, act_v = torch.max(q_vals_v, dim=0)
            action = int(act_v.item())

        # do step in the environment
        new_state, reward, is_done, _ = self.env.step(action)
        self.total_reward += reward
        #??
        new_state = new_state

        exp = Experience(self.state, action, reward,
                         is_done, new_state, self.adj)
        self.exp_buffer.append(exp)
        self.state = new_state
        if is_done:
            percentage = 1 - self.env.nodes_remain / self.env.n_nodes
            done_reward = self.total_reward
            self._reset()
        return done_reward, percentage


def calc_loss(batch, net, tgt_net, device="cpu"):
    states, actions, rewards, dones, next_states, adjs = batch

    #states_v = torch.FloatTensor(states).to(device)
    #next_states_v = torch.FloatTensor(next_states).to(device)
    actions_v = torch.tensor(actions).to(device)
    rewards_v = torch.tensor(rewards).to(device)
    done_mask = torch.ByteTensor(dones).to(device)

    state_action_values = torch.zeros(BATCH_SIZE, dtype=torch.float32)
    expected_state_action_values = torch.zeros(BATCH_SIZE, dtype=torch.float32)
    for i in range(BATCH_SIZE):
        state_v = torch.FloatTensor(states[i]).to(device)
        next_state_v = torch.FloatTensor(next_states[i]).to(device)
        #action = actions[i]
        action_v = actions_v[i]
        reward_v = rewards_v[i]
        adj = adjs[i]
        done = done_mask[i]

        state_action_values[i] = net(state_v, adj)[action_v]
        next_state_value = np.max(
            tgt_net(next_state_v, adj).detach().numpy())
        if done == True:
            next_state_value = 0.0
        expected_state_action_values[i] = next_state_value * GAMMA + reward_v

    return nn.MSELoss()(state_action_values, expected_state_action_values)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--cuda", default=False,
                        action="store_true", help="Enable cuda")
    parser.add_argument("--reward", type=float, default=MEAN_REWARD_BOUND,
                        help="Mean reward boundary for stop of training, default=%.2f" % MEAN_REWARD_BOUND)
    parser.add_argument("--log", type=str, default=None,
                        help="Name of log file to show on tensorboard")
    args = parser.parse_args()
    device = torch.device("cuda" if args.cuda else "cpu")

    n_colors = 3
    min_nodes = 5
    max_nodes = 6
    n_iter = 100
    n_nodes = [n_colors, 3, n_colors]

    env = GraphColoring(n_colors=n_colors,
                        min_nodes=min_nodes, max_nodes=max_nodes)

    #net = GCN(nfeat=n_colors, nhid=3, nclass=n_colors, dropout=False)
    net = ResGCN(n_nodes, dropout=True)
    #tgt_net = GCN(nfeat=n_colors, nhid=3, nclass=n_colors, dropout=True)
    tgt_net = ResGCN(n_nodes, dropout=True)
    adj, _ = env.reset()
    adj_dense = np.copy(adj)  # deep copy
    adj = sparse_mx_to_torch_sparse_tensor(
        normalize(sparse.csr_matrix(adj.astype(float)))).to(device)

    if args.log is None:
        writer = SummaryWriter()
    else:
        writer = SummaryWriter("runs/"+args.log)
    print(net)

    buffer = ExperienceBuffer(REPLAY_SIZE)
    agent = Agent(env, buffer, device)
    epsilon = EPSILON_START

    optimizer = optim.Adam(net.parameters(), lr=LEARNING_RATE)
    total_rewards = []
    total_percentages = []
    frame_idx = 0
    ts_frame = 0
    ts = time.time()
    best_mean_reward = None
    gif_dir = './'

    gv = GraphVisualizer(adj_dense, n_colors, gif_dir)

    for _ in range(STEP_NUM):
        frame_idx += 1
        epsilon = max(EPSILON_FINAL, EPSILON_START -
                      frame_idx / EPSILON_DECAY_LAST_FRAME)

#        if len(total_rewards) % 1000 == 0:
#            s = 'episode = ' + str(len(total_rewards))
#            gv.draw_graph(agent.state, text=[s])
#            gv.make_gif()
#
        reward, percentage = agent.play_step(net, epsilon)

        if reward is not None:
            total_rewards.append(reward)
            total_percentages.append(percentage)
            speed = (frame_idx - ts_frame) / (time.time() - ts)
            ts_frame = frame_idx
            ts = time.time()
            mean_reward = np.mean(total_rewards[-100:])
            mean_percentage = np.mean(total_percentages[-100:])
            print("%d: done %d games, mean reward %.3f, eps %.2f, mean filled percentage %.2f" % (
                frame_idx, len(
                    total_rewards), mean_reward, epsilon, mean_percentage
            ))
            writer.add_scalar("epsilon", epsilon, frame_idx)
            writer.add_scalar("speed", speed, frame_idx)
            writer.add_scalar("reward_100", mean_reward, frame_idx)
            writer.add_scalar("reward", reward, frame_idx)
            writer.add_scalar("percentage_100", mean_percentage, frame_idx)
            if best_mean_reward is None or best_mean_reward < mean_reward:
                torch.save(net.state_dict(), "graphcoloring-best.dat")
                if best_mean_reward is not None:
                    print("Best mean reward updated %.3f -> %.3f, model saved" %
                          (best_mean_reward, mean_reward))
                best_mean_reward = mean_reward
            if mean_reward > args.reward:
                print("Solved in %d frames!" % frame_idx)
                break

        if len(buffer) < REPLAY_START_SIZE:
            continue

        if frame_idx % SYNC_TARGET_FRAMES == 0:
            tgt_net.load_state_dict(net.state_dict())

        optimizer.zero_grad()
        batch = buffer.sample(BATCH_SIZE)
        loss_t = calc_loss(batch, net, tgt_net, device=device)
        loss_t.backward()
        optimizer.step()

    writer.close()
