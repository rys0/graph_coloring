import os
from datetime import datetime

import numpy as np
import pandas as pd
import networkx as nx

import imageio
import matplotlib.pyplot as plt
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure


class GraphVisualizer:
    def __init__(self, adj, n_colors, figdir, figname=datetime.now().strftime("%Y_%m_%d_%H_%M_%S"), layout=nx.spring_layout):
        self.adj = pd.DataFrame(adj)
        self.graph = nx.from_pandas_adjacency(self.adj)
        self.n_colors = n_colors
        self.n_nodes = len(adj)
        self.pos = layout(self.graph)
        if figdir[-1] != '/':
            figdir += '/'
        self.figdir = figdir
        self.figname = figname
        self.index = 0
        self.images = []

    def __repr__():
        pass

    def changeGraph(self, adj, n_colors):
        self.adj = pd.DataFrame(adj)
        self.graph = nx.from_pandas_adjacency(self.adj)
        self.n_colors = n_colors
        self.n_nodes = len(adj)
        self.pos = layout(self.graph)

    def default_index(self, node, x=True, default=0):
        if x in node:
            return np.where(node)[0][0] + 1
        else:
            return default

    def state2color(self, state):
        node_colors = []
        for node in state:
            i = self.default_index(node, True)
            node_colors.append(float(i) / self.n_colors)
        return np.array(node_colors)

    def draw_graph(self, state,  image_save=False, text=[]):
        node_color = self.state2color(state)
        fig = Figure()
        canvas = FigureCanvas(fig)
        ax = fig.gca()
        for i, t in enumerate(text):
            fig.text(0, i * 0.03, t)
        ax.axis('off')
        nx.draw_networkx_nodes(self.graph, self.pos, node_size=1000,
                               node_color=node_color, cmap='brg', vmin=0.0, vmax=1.0, ax=ax)
        nx.draw_networkx_edges(self.graph, self.pos, width=1, ax=ax)
        nx.draw_networkx_labels(self.graph, self.pos,
                                font_size=16, font_color="black", ax=ax)
        canvas.draw()
        image = np.fromstring(canvas.tostring_rgb(), dtype='uint8')
        width, height = fig.get_size_inches() * fig.get_dpi()
        image = np.reshape(
            image, (int(height), int(width), 3))
        self.images.append(image)
        if image_save == True:
            fig.savefig(self.figdir + self.figname + str(self.index) + '.png')
        self.index += 1

    def make_gif(self, duration=0.5):
        imageio.mimsave(self.figdir + self.figname +
                        'movie.gif', self.images, duration=duration)


if __name__ == '__main__':
    n_colors = 3
    n_nodes = 7

    adj = [[1, 1, 1, 0, 0, 1, 0],
           [1, 1, 0, 1, 1, 0, 1],
           [1, 0, 1, 0, 0, 0, 1],
           [0, 1, 0, 1, 0, 1, 0],
           [0, 1, 0, 0, 1, 0, 0],
           [1, 0, 0, 1, 0, 1, 1],
           [0, 1, 1, 0, 0, 1, 1]]

    state = np.array([[False] * n_colors] * n_nodes)

    gv = GraphVisualizer(adj, n_colors, './images')
    gv.draw_graph(state)
    state[0][0] = True
    gv.draw_graph(state)
    state[1][1] = True
    gv.draw_graph(state)
    state[2][2] = True
    gv.draw_graph(state)
    state[3][2] = True
    gv.draw_graph(state)
    state[4][0] = True
    gv.draw_graph(state)
    state[5][1] = True
    gv.draw_graph(state)
    state[6][0] = True
    gv.draw_graph(state)
    gv.make_gif()
