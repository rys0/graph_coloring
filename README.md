# graph_coloring

## Overview

Implementation of RL model that solves graph coloring problem using gcn.

## Dependencies

requirements.txtを参照せよ.

## Setup

    pip install -r requirements.txt

## Usage

implementaion/dqns 内で次のコマンドを実行する.

    python train.py

train.py内でparamsの各キーの値を変更することでパラメータなどの設定ができる.
各キーについてはimplementation/dqns/library/common.py及び項目Pramsの説明を参照せよ.

## Params

一部のキーについてのみ説明する.

- graoh_random  
Trueにすると可変のグラフで学習を行う.  
- n_colors  
彩色に使われる色の数.
- min_nodes  
生成されるグラフの最小ノード数.
- max_nodes
生成されるグラフの最大ノード数を設定するための値.
max_nodes-1が生成される最大ノード数になることに注意せよ.例えばmin_nodes=5, max_nodes=8ならばノード数5〜7のグラフが生成される.  
- test_freq  
何ステップごとにテストを行うかを決定する.  
- architecture  
モデルのレイヤ数やノード数を設定する.  
- learning_rate  
optimizerの学習率.
- gamma  
Q学習における割引率.
- reward_steps  
何ステップ先までの報酬を利用するかを設定する.
- gif  
Trueにするとテスト時の彩色の様子をテストごとにgif形式で出力する.

## Note

- 実装にあたり[Deep Reinforcement Learning Hands-On](https://www.packtpub.com/big-data-and-business-intelligence/deep-reinforcement-learning-hands)
のソースコードを参考にしている.

- [ptan](https://github.com/Shmuma/ptan)を改変したものを利用している.

## Author

Ryuta Shimogauchi  
Kazuma Michigami