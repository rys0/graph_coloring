#!/usr/bin/env python3
import os
import collections
import argparse
import time
import atexit
import copy

import gym
import numpy as np
from scipy import sparse
import pandas as pd

import torch
import torch.optim as optim
from tensorboardX import SummaryWriter
from torch import nn, optim
from ptan import ptan

from library import common
from library.models import GCN, ResGCN
from library.utils import normalize, sparse_mx_to_torch_sparse_tensor
from library.graph_visualizer import GraphVisualizer

def main(params):

    def record(result_path):
        architecture = params['architecture']
        params['architecture'] = ' '.join(str(e) for e in architecture)
        for k, v in params.items():
            params[k] = [v]
        record = pd.DataFrame.from_dict(params, orient='columns')
        record.to_csv(result_path, header=True)
                
    atexit.register(record, params['result_path'])

    device = torch.device("cuda" if params['cuda'] else "cpu")

    env = gym.make(params['env_name'])
    env.set_n_colors(params['n_colors'])
    env.set_n_node_range(params['min_nodes'], params['max_nodes'])
    env.reset()
    env.set_is_random(params['graph_random'])

    if params['graph_random'] == True:
        test_env = gym.make(params['env_name'])
        test_env.set_n_colors(params['n_colors'])
        test_env.set_n_node_range(params['min_nodes'], params['max_nodes'])
        test_env.reset()
        test_env.set_is_random(True)
    else:
        test_env = copy.deepcopy(env)

    if params['gif'] == True:
        if params['graph_random'] == True:
            fixed_test_env = gym.make(params['env_name'])
            fixed_test_env.set_n_colors(params['n_colors'])
            fixed_test_env.set_n_node_range(
                params['min_nodes'], params['max_nodes'])
            fixed_test_env.reset()
            fixed_test_env.set_is_random(False)
        else:
            fixed_test_env = copy.deepcopy(env)

        gv = GraphVisualizer(fixed_test_env.get_adj(), params['n_colors'],
                             params['figname'],)
    else:
        gv = None
        fixed_test_env = None

    writer = SummaryWriter(comment=params['summary_prefix'])

    if params['model'] == 'ResGCN':
        net = ResGCN(params['architecture'], dropout=True)
    elif params['model'] == 'GCN':
        net = GCN(nfeat=params['n_colors'], nhid=16, nclass=params['n_colors'], dropout=False)
        params['architecture'] = 'GCN'

    if not os.path.exists('./model'):
        os.makedirs('./model')
    if not os.path.exists('./gif'):
        os.makedirs('./gif')
    if not os.path.exists('./result'):
        os.makedirs('./result')
    
    tgt_net = ptan.agent.TargetNet(net)
    selector = ptan.actions.EpsilonGreedyActionSelector(
        epsilon=params['epsilon_start'])
    epsilon_tracker = common.EpsilonTracker(selector, params)
    agent = ptan.agent.DQNAgent(net, selector, device=device)
    exp_source = ptan.experience.ExperienceSourceFirstLast(
        env, agent, gamma=params['gamma'], steps_count=params['reward_steps'])
    buffer = ptan.experience.ExperienceReplayBuffer(
        exp_source, buffer_size=params['replay_size'])
    optimizer = optim.Adam(net.parameters(), lr=params['learning_rate'])
    frame_idx = 0
    
    with common.RewardTracker(writer, params['stop_reward'], params['model_path']) as reward_tracker:
        while True:
            frame_idx += 1
            params['frame_idx'] = frame_idx
            buffer.populate(1)
            epsilon_tracker.frame(frame_idx)

            new_rewards = exp_source.pop_total_rewards()
            new_percentages = exp_source.pop_percentages()
            if new_rewards:
                if reward_tracker.reward(new_rewards[0], new_percentages[0], frame_idx, net, params, selector.epsilon):
                    break

            if len(buffer) < params['replay_initial']:
                continue

            optimizer.zero_grad()
            batch = buffer.sample(params['batch_size'])
            loss_v = common.calc_loss_dqn(
                batch, net, tgt_net.target_model, gamma=params['gamma']**params['reward_steps'], device=device)
            loss_v.backward()
            mean_loss = loss_v.detach().mean().item()
            optimizer.step()

            writer.add_scalar('mean_loss', mean_loss, frame_idx)

            if frame_idx % params['test_freq'] == 0:
                mean_total_rewatd, mean_percentage = common.test(
                    net, test_env, frame_idx, params['test_num'], writer, fixed_test_env, gv, device)
                params['final_test_mean_total_reward'] = mean_total_rewatd
                params['final_test_mean_percetage'] = mean_percentage
                params['best_test_mean_total_reward'] = max(
                    mean_total_rewatd, params['best_test_mean_total_reward'])
                params['best_test_mean_percetage'] = max(
                    mean_percentage, params['best_test_mean_percetage'])

            if frame_idx % params['target_net_sync'] == 0:
                tgt_net.sync()
