# coding: utf-8
import torch.nn as nn
import torch.nn.functional as F
from .layers import GraphConvolution


class GCN(nn.Module):
    def __init__(self, nfeat, nhid, nclass, dropout):
        super(GCN, self).__init__()

        self.gc1 = GraphConvolution(nfeat, nhid)
        self.gc2 = GraphConvolution(nhid, nclass)
        self.dropout = dropout

    def forward(self, x):
        nodes = x[0]
        adj = x[1]
        y = F.relu(self.gc1(nodes, adj))
        y = F.dropout(y, self.dropout, training=self.training)
        y = self.gc2(y, adj)
        return y.view(-1)

    def set_adj(self, adj):
        self.__adj = adj


class ResGCN(nn.Module):
    def __init__(self, n_nodes, dropout):
        super(ResGCN, self).__init__()
        self.gcs = nn.ModuleList(
            [GraphConvolution(n_nodes[i], n_nodes[i + 1]) for i in range(len(n_nodes) - 1)])
        #self.fc = nn.Linear(n_nodes[0], n_nodes[-1])
        self.fcs = nn.ModuleList(
            [nn.Linear(n_nodes[i], n_nodes[i + 1]) for i in range(len(n_nodes) - 1)])
        self.dropout = dropout

    def forward(self, x):
        res = x[0]
        y = x[0]
        adj = x[1]
        for fc in self.fcs[:-1]:
            res = F.relu(fc(res))
            res = F.dropout(res, self.dropout, training=self.training)
        for gc in self.gcs[:-1]:
            y = F.relu(gc(y, adj))
            #y = F.dropout(y, self.dropout, training=self.training)
        y = self.gcs[-1](y, adj) + self.fcs[-1](res)
        return y.view(-1)


class ResGCN(nn.Module):
    def __init__(self, n_nodes, dropout):
        super(ResGCN, self).__init__()
        self.gcs = nn.ModuleList(
            [GraphConvolution(n_nodes[i], n_nodes[i + 1]) for i in range(len(n_nodes) - 1)])
        #self.fc = nn.Linear(n_nodes[0], n_nodes[-1])
        self.fcs = nn.ModuleList(
            [nn.Linear(n_nodes[i], n_nodes[i + 1]) for i in range(len(n_nodes) - 1)])
        self.dropout = dropout

    def forward(self, x):
        res = x[0]
        y = x[0]
        adj = x[1]
        for fc in self.fcs[:-1]:
            res = F.relu(fc(res))
            res = F.dropout(res, self.dropout, training=self.training)
        for gc in self.gcs[:-1]:
            y = F.relu(gc(y, adj))
            #y = F.dropout(y, self.dropout, training=self.training)
        y = self.gcs[-1](y, adj) + self.fcs[-1](res)
        return y.view(-1)


class ResGCN(nn.Module):
    def __init__(self, n_nodes, dropout):
        super(ResGCN, self).__init__()
        self.gcs = nn.ModuleList(
            [GraphConvolution(n_nodes[i], n_nodes[i + 1]) for i in range(len(n_nodes) - 1)])
        #self.fc = nn.Linear(n_nodes[0], n_nodes[-1])
        self.fcs = nn.ModuleList(
            [nn.Linear(n_nodes[i], n_nodes[i + 1]) for i in range(len(n_nodes) - 1)])
        self.dropout = dropout

    def forward(self, x):
        res = x[0]
        y = x[0]
        adj = x[1]
        for fc in self.fcs[:-1]:
            res = F.relu(fc(res))
            res = F.dropout(res, self.dropout, training=self.training)
        for gc in self.gcs[:-1]:
            y = F.relu(gc(y, adj))
            #y = F.dropout(y, self.dropout, training=self.training)
        y = self.gcs[-1](y, adj) + self.fcs[-1](res)
        return y.view(-1)


class NLayerGCN(nn.Module):
    def __init__(self, n_nodes, dropout):
        super(NLayerGCN, self).__init__()
        self.gcs = nn.ModuleList(
            [GraphConvolution(n_nodes[i], n_nodes[i + 1]) for i in range(len(n_nodes) - 1)])
        self.dropout = dropout

    def forward(self, x):
        res = x[0]
        y = x[0]
        adj = x[1]
        for fc in self.fcs[:-1]:
            res = F.relu(fc(res))
            res = F.dropout(res, self.dropout, training=self.training)
        y = self.gcs[-1](y, adj)
        return y.view(-1)
