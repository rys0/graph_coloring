import sys
import random

import numpy as np
from scipy import sparse

import gym
import gym.spaces

from library.utils import normalize, sparse_mx_to_torch_sparse_tensor


class GraphColoring(gym.Env):
    metadata = {'render.modes': ['human', 'ansi']}

    def __init__(self):
        super().__init__()
        self.reward_range = [-1, 1]
        self.is_random = True

    def set_n_node_range(self, min_nodes, max_nodes):
        self.min_nodes = min_nodes
        self.max_nodes = max_nodes

    def set_n_colors(self, n_colors):
        self.n_colors = n_colors

    def set_is_random(self, is_random):
        self.is_random = is_random

    def reset(self):
        if self.is_random == True:
            self.n_nodes = np.random.randint(self.min_nodes, self.max_nodes)
            self.adj = self.generate_graph(self.n_nodes)
            self.sparse_adj = sparse_mx_to_torch_sparse_tensor(
                normalize(sparse.csr_matrix(self.adj.astype(float)) +
                          sparse.eye(len(self.adj)))
            )
            self.action_space = gym.spaces.Discrete(
                self.n_nodes * self.n_colors)
            self.observation_space = gym.spaces.Box(
                low=0.0,
                high=1.0,
                shape=(self.n_nodes, self.n_colors),
                dtype=np.float32
            )

        self.nodes = np.array([[False] * self.n_colors] * self.n_nodes)
        self.nodes_remain = self.n_nodes

        return (self.nodes.astype(float)*2-1, self.sparse_adj)

    def step(self, action):
        """
        action: pos = action / n_colors, col = action % n_colors
        """
        is_done = True
        pos = action // self.n_colors
        col = action % self.n_colors
        if self.check_valid(pos, col):
            reward = 1 - self.nodes_remain/self.n_nodes
            self.nodes_remain -= 1
            self.nodes[pos, col] = True
            if self.nodes_remain != 0:
                is_done = False
        else:
            reward = -self.nodes_remain / self.n_nodes

        return (self.nodes.astype(float)*2-1, self.sparse_adj), reward, is_done, {}

    def render(self, mode='human', close=False):
        return None

    def close(self):
        pass

    def seed(self, seed=None):
        pass

    def check_valid(self, pos, color):
        """
        adj: グラフの隣接行列
        graph: 現在のグラフの状態(各ノードの色)
        pos: 置きたい場所
        color: 置きたい色
        """
        if self.nodes[pos].any():
            return False

        return not(self.nodes[self.adj[pos] == 1].any(axis=0)[color])

    def generate_graph(self, node_number, connectivity_rate=0.4):
        g = None
        while g is None or 0 in np.sum(g, axis=1):
            g = np.random.rand(node_number, node_number)
            g = g + g.T - 2 * np.eye(node_number)
            g = np.where(g > (1 - connectivity_rate) * 2, 1, 0)
        return g

    def get_adj(self):
        return self.adj

    def get_boolean_nodes(self):
        return self.nodes


if __name__ == '__main__':
    env = GraphColoring()
    print(env.generate_graph(5))
