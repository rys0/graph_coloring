from gym.envs.registration import register

register(
    id='GraphColoring-v0',
    entry_point='library.graph_coloring_env:GraphColoring',
)
