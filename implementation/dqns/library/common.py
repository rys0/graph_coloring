import sys
import time
import numpy as np
import torch
import torch.nn as nn
import statistics


HYPERPARAMS = {
    'graphcoloring': {
        'env_name': 'GraphColoring-v0',
        'stop_reward': 100000,
        'replay_size': 100000,
        'replay_initial': 10000,
        'target_net_sync':1000,
        'epsilon_frames': 10**5,
        'epsilon_start': 1.0,
        'epsilon_final': 0.02,
        'learning_rate': 0.0001,
        'gamma': 0.99,
        'batch_size': 32,
        'n_colors': 4,
        'min_nodes': 9,
        'max_nodes': 10,
        'model': "ResGCN",
        'architecture': [3, 16, 8, 4, 3],
        'test_freq': 10000,
        'test_num': 100,
        'cuda': False,
        'graph_random': True,
        'gif': False,
        'reward_steps': 1,
        'final_mean_total_reward': -1000000,
        'final_mean_percetage': -1000000,
        'best_mean_total_reward': -1000000,
        'best_mean_percetage': -1000000,
        'final_test_mean_total_reward': -1000000,
        'final_test_mean_percetage': -1000000,
        'best_test_mean_total_reward': -1000000,
        'best_test_mean_percetage': -1000000,
        'now':'default',
        'method': 'default',
        'summary_prefix': 'default',
        'result_path': 'default',
        'model_path': 'default',
        'figname': 'default',
        'frame_idx': -1
    }
}


def unpack_batch(batch):
    states, actions, rewards, dones, last_states = [], [], [], [], []
    for exp in batch:
        state = np.array(exp.state, copy=False)
        states.append(state)
        actions.append(exp.action)
        rewards.append(exp.reward)
        dones.append(exp.last_state is None)
        if exp.last_state is None:
            last_states.append(state)       # the result will be masked anyway
        else:
            last_states.append(np.array(exp.last_state, copy=False))
    return np.array(states, copy=False), np.array(actions), np.array(rewards, dtype=np.float32), \
           np.array(dones, dtype=np.uint8), np.array(last_states, copy=False)


def calc_loss_dqn(batch, net, tgt_net, gamma, device="cpu"):
    states, actions, rewards, dones, next_states = unpack_batch(batch)

    actions_v = torch.tensor(actions).to(device)
    rewards_v = torch.tensor(rewards).to(device)
    done_mask = torch.ByteTensor(dones).to(device)

    batch_size = len(batch)

    state_action_values = torch.zeros(batch_size, dtype=torch.float32).to(device)
    next_state_values = torch.zeros(batch_size, dtype=torch.float32).to(device)
    for i in range(batch_size):
        nodes_v = torch.FloatTensor(states[i][0]).to(device)
        adj_v = states[i][1].to(device)
        state_v = (nodes_v, adj_v)
        next_nodes_v = torch.FloatTensor(next_states[i][0]).to(device)
        next_adj_v = next_states[i][1].to(device)
        next_state_v = (next_nodes_v, next_adj_v)
        action_v = actions_v[i]

        state_action_values[i] = net(state_v)[action_v]
        next_state_values[i] = tgt_net(next_state_v).max(0)[0]

    next_state_values[done_mask] = 0.0

    next_state_values.to(device)
    expected_state_action_values = next_state_values.detach() * gamma + rewards_v
    return nn.MSELoss()(state_action_values, expected_state_action_values)

def test_once(net, env, frame_idx, gv=None, device='cpu'):
    net.eval()
    state = env.reset()
    texts = ['step = ' + str(frame_idx)]
    is_done = False
    total_reward = 0
    while is_done != True:
        if gv != None:
            gv.draw_graph(env.get_boolean_nodes(), texts=texts)
            
        node_v = torch.FloatTensor(state[0]).to(device)
        adj_v = state[1].to(device)
        state_v = (node_v, adj_v)
        action = net(state_v).max(0)[1]
        action = int(action.item())
        next_state, reward, is_done, _ = env.step(action)
        state = next_state
        total_reward += reward
    
    if gv!= None:
        texts.append('Done')
        gv.draw_graph(env.get_boolean_nodes(), texts=texts)
        gv.make_gif()

    percentage = 1 - env.nodes_remain / env.n_nodes
    net.train()
    
    return total_reward, percentage


def test(net, test_env, frame_idx, test_num, writer, fixed_test_env=None, gv=None, device='cpu'):
    
    total_rewards = []
    percentages = []
    for _ in range(test_num):
        total_reward, percentage = test_once(net, test_env, frame_idx, device=device)
        total_rewards.append(total_reward)
        percentages.append(percentage)
        
    if fixed_test_env != None and gv != None:
        test_once(net, fixed_test_env, frame_idx, gv, device)
    mean_total_reward = np.mean(total_rewards)
    mean_percetage = np.mean(percentages)

    writer.add_scalar('test_total_rewatd', mean_total_reward, frame_idx)
    writer.add_scalar('test_percentage', mean_percetage, frame_idx)

    return mean_total_reward, mean_percetage

class RewardTracker:
    def __init__(self, writer, stop_reward, model_name):
        self.writer = writer
        self.stop_reward = stop_reward
        self.best_mean_reward = None
        self.model_name = model_name
        self.last_mean_reward = None
        self.last_mean_percentage = None

    def __enter__(self):
        self.ts = time.time()
        self.ts_frame = 0
        self.total_rewards = []
        self.percentages = []
        return self

    def __exit__(self, *args):
        self.writer.close()

    def reward(self, reward, percentage, frame, net, params, epsilon=None):
        self.total_rewards.append(reward)
        self.percentages.append(percentage)
        speed = (frame - self.ts_frame) / (time.time() - self.ts)
        self.ts_frame = frame
        self.ts = time.time()
        mean_reward = np.mean(self.total_rewards[-100:])
        mean_percentage = np.mean(self.percentages[-100:])
        epsilon_str = "" if epsilon is None else ", eps %.2f" % epsilon
        print("%d: done %d games, mean reward %.3f, speed %.2f f/s%s, filled percentage %.2f" % (
            frame, len(self.total_rewards), mean_reward, speed, epsilon_str, mean_percentage
        ))
        sys.stdout.flush()
        if epsilon is not None:
            self.writer.add_scalar("epsilon", epsilon, frame)
        self.writer.add_scalar("speed", speed, frame)
        self.writer.add_scalar("reward_100", mean_reward, frame)
        self.writer.add_scalar("reward", reward, frame)
        self.writer.add_scalar("percentage_100", mean_percentage, frame)

        params['final_mean_total_reward'] = mean_reward
        params['final_mean_percetage'] = mean_percentage

        if self.best_mean_reward is None or self.best_mean_reward < mean_reward:
            torch.save(net.state_dict(), self.model_name)
            if self.best_mean_reward is not None:
                print("Best mean reward updated %.3f -> %.3f, model saved" %
                      (self.best_mean_reward, mean_reward))
            self.best_mean_reward = mean_reward
            self.writer.add_scalar(
                "best_mean_reward", self.best_mean_reward, frame)
            self.writer.add_scalar(
                "best_mean_percentage", mean_percentage, frame)
            params['best_mean_total_reward'] = self.best_mean_reward
            params['best_mean_percetage'] = mean_percentage


        if mean_reward > self.stop_reward:
            print("Solved in %d frames!" % frame)
            return True
        return False


class EpsilonTracker:
    def __init__(self, epsilon_greedy_selector, params):
        self.epsilon_greedy_selector = epsilon_greedy_selector
        self.epsilon_start = params['epsilon_start']
        self.epsilon_final = params['epsilon_final']
        self.epsilon_frames = params['epsilon_frames']
        self.frame(0)

    def frame(self, frame):
        self.epsilon_greedy_selector.epsilon = \
            max(self.epsilon_final, self.epsilon_start - frame / self.epsilon_frames)


def distr_projection(next_distr, rewards, dones, Vmin, Vmax, n_atoms, gamma):
    """
    Perform distribution projection aka Catergorical Algorithm from the
    "A Distributional Perspective on RL" paper
    """
    batch_size = len(rewards)
    proj_distr = np.zeros((batch_size, n_atoms), dtype=np.float32)
    delta_z = (Vmax - Vmin) / (n_atoms - 1)
    for atom in range(n_atoms):
        tz_j = np.minimum(Vmax, np.maximum(Vmin, rewards + (Vmin + atom * delta_z) * gamma))
        b_j = (tz_j - Vmin) / delta_z
        l = np.floor(b_j).astype(np.int64)
        u = np.ceil(b_j).astype(np.int64)
        eq_mask = u == l
        proj_distr[eq_mask, l[eq_mask]] += next_distr[eq_mask, atom]
        ne_mask = u != l
        proj_distr[ne_mask, l[ne_mask]] += next_distr[ne_mask, atom] * (u - b_j)[ne_mask]
        proj_distr[ne_mask, u[ne_mask]] += next_distr[ne_mask, atom] * (b_j - l)[ne_mask]
    if dones.any():
        proj_distr[dones] = 0.0
        tz_j = np.minimum(Vmax, np.maximum(Vmin, rewards[dones]))
        b_j = (tz_j - Vmin) / delta_z
        l = np.floor(b_j).astype(np.int64)
        u = np.ceil(b_j).astype(np.int64)
        eq_mask = u == l
        eq_dones = dones.copy()
        eq_dones[dones] = eq_mask
        if eq_dones.any():
            proj_distr[eq_dones, l] = 1.0
        ne_mask = u != l
        ne_dones = dones.copy()
        ne_dones[dones] = ne_mask
        if ne_dones.any():
            proj_distr[ne_dones, l] = (u - b_j)[ne_mask]
            proj_distr[ne_dones, u] = (b_j - l)[ne_mask]
    return proj_distr
