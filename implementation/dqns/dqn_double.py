#!/usr/bin/env python3
import collections
import argparse
import time

import gym
import gym.spaces
import numpy as np
from scipy import sparse
import torch
import torch.optim as optim
from tensorboardX import SummaryWriter
from torch import nn, optim
from ptan import ptan

from library import common
from library.models import GCN, ResGCN
from library.utils import normalize, sparse_mx_to_torch_sparse_tensor
from library.graph_visualizer import GraphVisualizer

STATES_TO_EVALUATE = 1000
EVAL_EVERY_FRAME = 100


def calc_loss(batch, net, tgt_net, gamma, device="cpu", double=True):
    states, actions, rewards, dones, next_states = common.unpack_batch(batch)

    actions_v = torch.tensor(actions).to(device)
    rewards_v = torch.tensor(rewards).to(device)
    done_mask = torch.ByteTensor(dones).to(device)

    batch_size = len(batch)

    state_action_values = torch.zeros(batch_size, dtype=torch.float32).to(device)
    next_state_values = torch.zeros(batch_size, dtype=torch.float32).to(device)
    for i in range(batch_size):
        nodes_v = torch.FloatTensor(states[i][0]).to(device)
        adj_v = states[i][1].to(device)
        state_v = (nodes_v, adj_v)
        next_nodes_v = torch.FloatTensor(next_states[i][0]).to(device)
        next_adj_v = next_states[i][1].to(device)
        next_state_v = (next_nodes_v, next_adj_v)
        action_v = actions_v[i]
        
        state_action_values[i] = net(state_v)[action_v]
        if double:
            next_state_action = net(next_states_v).max(0)[1]
            next_state_values[i] = tgt_net(next_states_v)[next_state_action]
        else:
            next_state_values[i] = tgt_net(next_state_v).max(0)[0]
        
    next_state_values[done_mask] = 0.0

    next_state_values.to(device)
    expected_state_action_values = next_state_values.detach() * gamma + rewards_v

    return nn.MSELoss()(state_action_values, expected_state_action_values)

    

def calc_values_of_states(states, net, device="cpu"):
    mean_vals = []
    for state in states:
        nodes_v = torch.FloatTensor(state[0]).to(device)
        adj_v = state[1].to(device)
        state_v = (nodes_v, adj_v)
        action_value_v = net(state_v)
        best_action_value_v = action_value_v.max(0)[0]
        mean_vals.append(best_action_value_v.cpu().detach().numpy())
    return np.mean(mean_vals)


def main(params=None):
    params = common.HYPERPARAMS['graphcoloring']
    parser = argparse.ArgumentParser()
    parser.add_argument("--cuda", default=False, action="store_true", help="Enable cuda")
    parser.add_argument("--double", default=False, action="store_true", help="Enable double DQN")
    parser.add_argument("--gif", default=False,
                        action="store_true", help="If True, make gif")
    args = parser.parse_args()
    device = torch.device("cuda" if args.cuda else "cpu")

    env = gym.make(params['env_name'])
    test_env = gym.make(params['env_name'])
    test_env.set_is_random(False)

    writer = SummaryWriter(comment="-" + "-double")
    n_colors = env.observation_space.shape[1]
    net = GCN(nfeat=n_colors, nhid=3, nclass=n_colors,
              dropout=False).to(device)
    tgt_net = ptan.agent.TargetNet(net)
    selector = ptan.actions.EpsilonGreedyActionSelector(
        epsilon=params['epsilon_start'])
    epsilon_tracker = common.EpsilonTracker(selector, params)
    agent = ptan.agent.DQNAgent(net, selector, device=device)

    tgt_net = ptan.agent.TargetNet(net)
    selector = ptan.actions.EpsilonGreedyActionSelector(epsilon=params['epsilon_start'])
    epsilon_tracker = common.EpsilonTracker(selector, params)
    agent = ptan.agent.DQNAgent(net, selector, device=device)

    exp_source = ptan.experience.ExperienceSourceFirstLast(env, agent, gamma=params['gamma'], steps_count=1)
    buffer = ptan.experience.ExperienceReplayBuffer(exp_source, buffer_size=params['replay_size'])
    optimizer = optim.Adam(net.parameters(), lr=params['learning_rate'])

    frame_idx = 0
    eval_states = None

    if args.gif == True:
        gv = GraphVisualizer(env.get_dense_adj(), n_colors, './gif')
    else:
        gv = None

    with common.RewardTracker(writer, params['stop_reward']) as reward_tracker:
        while True:
            frame_idx += 1
            buffer.populate(1)
            epsilon_tracker.frame(frame_idx)

            new_rewards = exp_source.pop_total_rewards()
            new_percentages = exp_source.pop_percentages()
            if new_rewards:
                if reward_tracker.reward(new_rewards[0], new_percentages[0], frame_idx, selector.epsilon):
                    break

            if len(buffer) < params['replay_initial']:
                continue
            #if eval_states is None:
            #    eval_states = buffer.sample(STATES_TO_EVALUATE)
            #    eval_states = [np.array(transition.state, copy=False) for transition in eval_states]
            #    eval_states = np.array(eval_states, copy=False)

            optimizer.zero_grad()
            batch = buffer.sample(params['batch_size'])
            loss_v = calc_loss(batch, net, tgt_net.target_model, gamma=params['gamma'], device=device,
                               double=args.double)
            loss_v.backward()
            optimizer.step()

            if frame_idx % params['test_freq'] == 0:
                common.test_dqn(net, test_env, frame_idx, gv)

            if frame_idx % params['target_net_sync'] == 0:
                tgt_net.sync()

            #if frame_idx % EVAL_EVERY_FRAME == 0:
            #    mean_val = calc_values_of_states(eval_states, net, device=device)
            #    writer.add_scalar("values_mean", mean_val, frame_idx)


if __name__ == "__main__":
    params = common.HYPERPARAMS['graphcoloring']
    parser = argparse.ArgumentParser()
    parser.add_argument("--cuda", default=False,
                        action="store_true", help="Enable cuda")
    parser.add_argument("--gif", default=False,
                        action="store_true", help="If True, make gif")
    args = parser.parse_args()

    params['cuda'] = args.cuda
    params['gif'] = args.gif
    params['epsilon_frames'] = 200000

    main(params)
