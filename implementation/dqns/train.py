from datetime import datetime

import dqn_n_steps
from library import common

if __name__ == '__main__':
    params = common.HYPERPARAMS['graphcoloring']
    params['graph_random'] = True
    params['n_colors'] = 4
    params['min_nodes'] = 9
    params['max_nodes'] = 10
    params['batch_size'] = 2
    params['test_freq'] = 1000
    params['architecture'] = [params['n_colors'], 16, 8, 4, params['n_colors']]
    params['learning_rate'] = 0.0001
    params['gamma'] = 0.99
    params['reward_steps'] = 2

    params['now'] = datetime.now().strftime("%Y_%m_%d_%H_%M_%S")
    params['method'] = 'n_steps'
    params['gif'] = False
    params['cuda'] = False
    params['result_path'] = './result/' + \
        params['now'] + params['method'] + '_result.csv'
    params['summary_prefix'] = params['now'] + "-" + params['method']
    params['model_path'] = './model/' + \
        params['now'] + params['method'] + '.dat'
    params['figname'] = './gif/' + params['now']
    dqn_n_steps.main(params)
