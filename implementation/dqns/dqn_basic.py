#!/usr/bin/env python3
import os
from datetime import datetime 
import collections
import argparse
import time
import atexit

import gym
import gym.spaces
import numpy as np
from scipy import sparse
import pandas as pd

import torch
import torch.optim as optim
from tensorboardX import SummaryWriter
from torch import nn, optim
from ptan import ptan

from library import common
from library.models import GCN, ResGCN
from library.utils import normalize, sparse_mx_to_torch_sparse_tensor
from library.graph_visualizer import GraphVisualizer

def main(params):

    def record():
        architecture = params['architecture']
        params['architecture'] = ' '.join(str(e) for e in architecture)
        for k, v in params.items():
            params[k] = [v]
        record = pd.DataFrame.from_dict(params, orient='columns')
        record.to_csv('result.csv', mode='a', header=True)
    atexit.register(record)

    device = torch.device("cuda" if params['cuda'] else "cpu")

    env = gym.make(params['env_name'])
    env.set_n_colors(params['n_colors'])
    env.set_is_random(params['graph_random'])
    env.set_n_node_range(params['min_nodes'], params['max_nodes'])
    env.reset()
    test_env = gym.make(params['env_name'])
    test_env.set_n_colors(params['n_colors'])
    test_env.set_is_random(params['graph_random'])
    test_env.set_n_node_range(params['min_nodes'], params['max_nodes'])

    params['now'] = datetime.now().strftime("%Y_%m_%d_%H_%M_%S")
    params['method'] = 'basic'

    params['log_comment'] = params['now'] + "-" + params['method']
    writer = SummaryWriter(comment=params['log_comment'])

    net = ResGCN(params['architecture'], dropout=True)

    if not os.path.exists('./model'):
        os.makedirs('./model')
    if not os.path.exists('./gif'):
        os.makedirs('./gif')

    model_name = './model/' + params['now'] + params['method'] + '.dat'
    
    if params['gif'] == True:
        gv = GraphVisualizer(env.get_dense_adj(), params['n_colors'],
                             './gif' + params['now'], ) if params['gif'] == True else None
        fixed_test_env = gym.make(params['env_name'])
        fixed_test_env.set_n_colors(params['n_colors'])
        fixed_test_env.set_is_random(False)
        fixed_test_env.set_n_node_range(params['min_nodes'], params['max_nodes'])
    else:
        gv = None
        fixed_test_env = None
    

    tgt_net = ptan.agent.TargetNet(net)
    selector = ptan.actions.EpsilonGreedyActionSelector(
        epsilon=params['epsilon_start'])
    epsilon_tracker = common.EpsilonTracker(selector, params)
    agent = ptan.agent.DQNAgent(net, selector, device=device)
    exp_source = ptan.experience.ExperienceSourceFirstLast(
        env, agent, gamma=params['gamma'], steps_count=1)
    buffer = ptan.experience.ExperienceReplayBuffer(
        exp_source, buffer_size=params['replay_size'])
    optimizer = optim.Adam(net.parameters(), lr=params['learning_rate'])
    frame_idx = 0
    
    result = params

    with common.RewardTracker(writer, params['stop_reward'], model_name) as reward_tracker:
        while True:
            frame_idx += 1
            buffer.populate(1)
            epsilon_tracker.frame(frame_idx)

            new_rewards = exp_source.pop_total_rewards()
            new_percentages = exp_source.pop_percentages()
            if new_rewards:
                if reward_tracker.reward(new_rewards[0], new_percentages[0], frame_idx, net, selector.epsilon):
                    break

            if len(buffer) < params['replay_initial']:
                continue

            optimizer.zero_grad()
            batch = buffer.sample(params['batch_size'])
            loss_v = common.calc_loss_dqn(
                batch, net, tgt_net.target_model, gamma=params['gamma'], device=device)
            loss_v.backward()
            mean_loss = loss_v.detach().mean().item()
            optimizer.step()

            writer.add_scalar('mean_loss', mean_loss, frame_idx)

            if frame_idx % params['test_freq'] == 0:
                mean_total_rewatd, mean_percentage = common.test(
                    net, test_env, frame_idx, params['test_num'], writer, fixed_test_env, gv, device)
                params['final_test_mean_total_reward'] = mean_total_rewatd
                params['final_test_mean_percetage'] = mean_percentage
                params['best_test_mean_total_reward'] = max(
                    mean_total_rewatd, params['best_test_mean_total_reward'])
                params['best_test_mean_percetage'] = max(
                    mean_percentage, params['best_test_mean_percetage'])

            if frame_idx % params['target_net_sync'] == 0:
                tgt_net.sync()
    

if __name__ == '__main__':
    params = common.HYPERPARAMS['graphcoloring']
    parser = argparse.ArgumentParser()
    parser.add_argument("--cuda", default=False,
                        action="store_true", help="Enable cuda")
    parser.add_argument("--gif", default=False,
                        action="store_true", help="If True, make gif")
    args = parser.parse_args()

    params['cuda'] = args.cuda
    params['gif'] = args.gif
    params['epsilon_frames'] = 200000

    main(params)
